# MCoreUtils conda recipe

Home: https://github.com/epics-modules/MCoreUtils

Package license: EPICS Open License

Recipe license: BSD 3-Clause

Summary: EPICS MCoreUtils module
